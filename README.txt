
This module allows administrator to configure content to be displayed on mouse hover for a Drupal menu item from Block, View, Node or 
dispaly submenu . When a Drupal menu is enabled for Mega-Dropdown, it shows Mega Dropdown configuration block on edit page of each menu item of that menu. The administrator can choose to display content in dropdown from any source - Block, Node, View or display the submenu of current menu item.
Additionally, administrator can choose NOT to display any content. All the Mega Dropdown menu expose a new block with name of menu preceded by Mega Dropdown ie, Mega Dropdown: menu-megadropdown where mega-dropdown is name of Drupal menu enabled as Mega Dropdown.


Installation
------------

1. Add the module to your modules directory.
2. Enable the module.
3. Visit Mega dropdown Settings page (i.e, admin/settings/megadropdown)to enable menu for Mega Dropdown.
4. On Drupal blocks page, a new block is added with name Mega Dropdown:<Drupal menu name>.
5. Configure it to show you want to display your Mega Dropdown.
6. On Menu edit page, for each menu item, configure the Mega dropdown setting to show your desired Dropdown.


Author
------
Ravi Gupta
http://drupal.org/user/446112
ravigup@gmail.com