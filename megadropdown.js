$(document).ready(function() {

  function megaHoverOver(){
  
    $(this).children(".megadropdown-dropdown").stop().fadeTo('fast', 1).show();
    $(this).addClass('hover-menu');
			
    //Calculate width of all ul's
    (function($) {
      jQuery.fn.calcSubWidth = function() {
        rowWidth = 0;
        //Calculate row
        $(this).find(".sub").each(function() {
          rowWidth += $(this).width();
        });
      };
    })(jQuery);
		
    /*if ( $(this).find(".row").length > 0 ) { //If row exists...
			var biggestRow = 0;	
			//Calculate each row
			$(this).find(".row").each(function() {							   
				$(this).calcSubWidth();
				//Find biggest row
				if(rowWidth > biggestRow) {
					biggestRow = rowWidth;
				}
			});
			//Set width
			$(this).find(".sub").css({'width' :biggestRow});
			$(this).find(".row:last").css({'margin':'0'});
			
		} else */
    { //If row does not exist...
      rowWidth=$(this).find(".sub").attr('dropdownwidth')
      if(rowWidth) {
        $(this).find(".sub").css({
          'width' : rowWidth
        });
      }
    }
  }
	
  function megaHoverOut(){
    $(this).children(".megadropdown-dropdown").stop().fadeTo('fast', 0, function() {
      $(this).hide();
    });
    $(this).removeClass('hover-menu');
  }
  var config = {
    sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)
    interval: 100, // number = milliseconds for onMouseOver polling interval
    over: megaHoverOver, // function = onMouseOver callback (REQUIRED)
    timeout: 500, // number = milliseconds delay before onMouseOut
    out: megaHoverOut // function = onMouseOut callback (REQUIRED)
  };

  $("ul.megadropdown-topnav li .sub").css({
    'opacity':'0'
  });
  $("ul.megadropdown-topnav li").hoverIntent(config);
});